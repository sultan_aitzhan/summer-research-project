##########################################################################################################################################################
#Date created: August 20, 2019
#Name: Sultan Aitzhan
#Description: The relevant code for constructing an adjoint matrix via ApproxFun package ##########################################################################################################################################################
# Importing packages
##########################################################################################################################################################
using PyCall
using ApproxFun
using StrLiterals
import LinearAlgebra
using IntervalSets

##########################################################################################################################################################
# Global Constants
TOL = 1e-05
DIGITS = 3 

##########################################################################################################################################################
# Helper functions
##########################################################################################################################################################
function check_all(array, condition)
    for x in array
        if !condition(x)
            return false
        end
    end
    return true
end

function check_any(array, condition)
    for x in array
        if condition(x)
            return true
        end
    end
    return false
end

function set_tol(x::Union{Number, Array}, y::Union{Number, Array}; atol = TOL)
    if isa(x, Number) && isa(y, Number)
       return atol * mean([x y])
    elseif isa(x, Array) && isa(y, Array)
        if size(x) != size(y)
            throw(error("Array dimensions do not match"))
        end
        # Avoid InexactError() when taking norm()
        x = convert(Array{Complex}, x)
        y = convert(Array{Complex}, y)
        return atol * (LinearAlgebra.norm(x) + LinearAlgebra.norm(y))
    else
        throw(error("Invalid input"))
    end
end

function evaluate(func::Union{Function, Number}, a::Number)
    if isa(func, Function)
        funcA = func(a)
    else # func is Number
        funcA = func
    end
    return funcA
end

function add_func(f::Union{Number, Function}, g::Union{Number, Function})
    function h(x)
        if isa(f, Number)
            if isa(g, Number)
                return f + g
            else
                return f + g(x)
            end
        elseif isa(f, Function)
            if isa(g, Number)
                return f(x) + g
            else
                return f(x) + g(x)
            end
        end
    end
    return h
end

function mult_func(f::Union{Number, Function}, g::Union{Number, Function})
    function h(x)
        if isa(f, Number)
            if isa(g, Number)
                return f * g
            else
                return f * g(x)
            end
        elseif isa(f, Function)
            if isa(g, Number)
                return f(x) * g
            else
                return f(x) * g(x)
            end
        end
    end
    return h
end

function is_approxLess(x::Number, y::Number; atol = TOL)
    if isa(x, Sym) == true || isa(y, Sym) == true
        x = convert(Float64, x)
        y = convert(Float64, x)
    end
    return !isapprox(x, y; atol = atol) && x < y
end

function is_approx(x::Number, y::Number; atol = TOL)
    if isa(x, Sym) == true || isa(y, Sym) == true
        x = convert(Float64, x)
        y = convert(Float64, x)
    end
    return isapprox(x, y; atol = atol)
end

function FunctionConjugate(func::Union{Fun, Number}, interval)
    if typeof(func) == Fun
        function funcConj(x)
            return conj(func(x))
        end
        interval = domain(func)
        coeffs = size(coefficients(func))[1]
        return Fun(funcConj, interval, coeffs)
    else 
        return Fun(conj(func), interval)
    end
end

function ConvertFunArray(Arr::Fun)
    n = length(Arr)
    newArr = Array{Any}(undef, 1, n)
    for i = 1:n
        newArr[i] = Arr[i]
    end
    return newArr
end

##########################################################################################################################################################
# Structs
##########################################################################################################################################################
# A struct definition error type is the class of all errors in a struct definition
struct StructDefinitionError <: Exception
    msg::String
end

struct LinearDifferentialOperator
    pFunctions::Array # Array or ArraySpace of ApproxFun functions
    interval::IntervalSets.AbstractInterval{TYPinterval} where TYPinterval<:Real
    LinearDifferentialOperator(pFunctions::Array, interval::Interval{:closed,:closed}) =
    try
        L = new(pFunctions, interval)
        check_linearDifferentialOperator_input(L)
        return L
    catch err
        throw(err)
    end
end

# Check whether the inputs of L are valid.
function check_linearDifferentialOperator_input(L::LinearDifferentialOperator)
    pFunctions, interval = L.pFunctions, L.interval
    if !check_all(pFunctions, pFunc -> (isa(pFunc, Fun) || isa(pFunc, Number))) # check whether functions are ApproxFun objects
        throw(StructDefinitionError(:"p_k should be Fun or Number"))
    elseif !check_all(pFunctions, pFunc -> (interval == domain(pFunc)))
        throw(StructDefinitionError(:"Intervals should be the same"))
    else
        return true
    end
end


# A boundary condition Ux = 0 is encoded by an ordered pair of two matrices (M, N) whose entries are Numbers.

struct VectorBoundaryForm
    M::Array # Why can't I specify Array{Number,2} without having a MethodError?
    N::Array # We can also use Matrix{<:Number}, and so we will need to alter the below checker
    VectorBoundaryForm(M::Array, N::Array) =
    try
        U = new(M, N)
        check_vectorBoundaryForm_input(U)
        return U
    catch err
        throw(err)
    end
end

# Check whether the input matrices that characterize U are valid
function check_vectorBoundaryForm_input(U::VectorBoundaryForm)
    # M, N = U.M, U.N
    # Avoid Inexact() error when taking rank()
    M = convert(Array{Complex}, U.M)
    N = convert(Array{Complex}, U.N)
    if !(check_all(U.M, x -> isa(x, Number)) && check_all(U.N, x -> isa(x, Number)))
        throw(StructDefinitionError(:"Entries of M, N should be Number"))
    elseif size(U.M) != size(U.N)
        throw(StructDefinitionError(:"M, N dimensions do not match"))
    elseif size(U.M)[1] != size(U.M)[2]
        throw(StructDefinitionError(:"M, N should be square matrices"))
    elseif LinearAlgebra.rank(hcat(M, N)) != size(M)[1] # rank() throws weird "InexactError()" when taking some complex matrices
        throw(StructDefinitionError(:"Boundary operators not linearly independent"))
    else
        return true
    end
end

##########################################################################################################################################################
# Functions
##########################################################################################################################################################
function get_URank(U::VectorBoundaryForm)
    # Avoid InexactError() when taking hcat() and rank()
    M = convert(Array{Complex}, U.M)
    N = convert(Array{Complex}, U.N)
    MHcatN = hcat(M, N)
    return LinearAlgebra.rank(MHcatN)
end

function get_Uc(U::VectorBoundaryForm)
    try
        check_vectorBoundaryForm_input(U)
        n = get_URank(U)
        I = complex(Matrix{Float64}(LinearAlgebra.I, 2n, 2n))
        M, N = U.M, U.N
        MHcatN = hcat(M, N)
        # Avoid InexactError() when taking rank()
        mat = convert(Array{Complex}, MHcatN)
        for i = 1:(2*n)
            newMat = vcat(mat, I[i:i,:])
            newMat = convert(Array{Complex}, newMat)
            if LinearAlgebra.rank(newMat) == LinearAlgebra.rank(mat) + 1
                mat = newMat
            end
        end
        UcHcat = mat[(n+1):(2n),:]
        Uc = VectorBoundaryForm(UcHcat[:,1:n], UcHcat[:,(n+1):(2n)])
        return Uc
    catch err
        return err
    end
end

# Construct H from M, N, Mc, Nc
function get_H(U::VectorBoundaryForm, Uc::VectorBoundaryForm)
    MHcatN = hcat(convert(Array{Complex}, U.M), convert(Array{Complex}, U.N))
    McHcatNc = hcat(convert(Array{Complex}, Uc.M), convert(Array{Complex}, Uc.N))
    H = vcat(MHcatN, McHcatNc)
    return H
end

function get_pDerivMatrix(L::LinearDifferentialOperator)
    pFunctions = L.pFunctions
    n = length(pFunctions)-1
    pDerivMatrix = Array{Fun}(undef, n,n)
    D = Derivative()
    for i in 1:n
        for j in 1:n
            if j == 1
                pDerivMatrix[i,j] = pFunctions[i]
            else
                pDerivMatrix[i,j] = (D^(j-1))*pFunctions[i]
            end
        end
    end
    return pDerivMatrix
end

function get_Bjk(L::LinearDifferentialOperator, j::Int, k::Int; pDerivMatrix = get_pDerivMatrix(L))
    n = length(L.pFunctions)-1
    if j <= 0 || j > n || k <= 0 || k > n
        throw("j, k should be in {1, ..., n}")
    end
    sum = 0
    for l = (j-1):(n-k)
        summand = mult_func(binomial(l, j-1) * (-1)^l, pDerivMatrix[n-k-l+1, l-j+1+1])
        sum = add_func(sum, summand)
    end
    return sum
end

function get_B(L::LinearDifferentialOperator; pDerivMatrix = get_pDerivMatrix(L))
    n = length(L.pFunctions)-1
    B = Array{Any}(undef, n, n)
    for j = 1:n
        for k = 1:n
            B[j,k] = get_Bjk(L, j, k; pDerivMatrix = pDerivMatrix)
        end
    end
    return B
end

function get_BHat(L::LinearDifferentialOperator, B::Array)
    pFunctions, interval = L.pFunctions, L.interval
    (a,b) = endpoints(interval)
    n = length(pFunctions)-1
    BHat = Array{Float64, 2}(undef, 2n, 2n)
    BHat = convert(Array{Complex}, BHat)
    BEvalA = evaluate.(B, a)
    BEvalB = evaluate.(B, b)
    BHat[1:n,1:n] = -BEvalA
    BHat[(n+1):(2n),(n+1):(2n)] = BEvalB
    BHat[1:n, (n+1):(2n)] = zeros(n, n)
    BHat[(n+1):(2n), 1:n] = zeros(n, n)
    return BHat
end

function get_J(BHat, H)
    n = size(H)[1]
    H = convert(Array{Complex}, H)
    J = (BHat * inv(H))'
    # J = convert(Array{Complex}, J)
    return J
end

function get_adjointCand(J) # Adjoint Candidate
    n = convert(Int, size(J)[1]/2)
    J = convert(Array{Complex}, J)
    PStar = J[(n+1):2n,1:n]
    QStar = J[(n+1):2n, (n+1):2n]
    adjointU = VectorBoundaryForm(PStar, QStar)
    return adjointU
end

function check_adjoint(L::LinearDifferentialOperator, U::VectorBoundaryForm, adjointU::VectorBoundaryForm, B::Array)
    (a, b) = endpoints(L.interval)
    M, N = U.M, U.N
    P, Q = (adjointU.M)', (adjointU.N)'
    # Avoid InexactError() when taking inv()
    BEvalA = convert(Array{Complex}, evaluate.(B, a))
    BEvalB = convert(Array{Complex}, evaluate.(B, b))
    left = M * inv(BEvalA) * P
    right = N * inv(BEvalB) * Q

    tol = set_tol(left, right)
    return all(i -> isapprox(left[i], right[i]; atol = tol), 1:length(left)) # Can't use == to deterimine equality because left and right are arrays of floats
end

function get_adjointU(L::LinearDifferentialOperator, U::VectorBoundaryForm, pDerivMatrix=get_pDerivMatrix(L))
    B = get_B(L; pDerivMatrix = pDerivMatrix)
    BHat = get_BHat(L, B)
    Uc = get_Uc(U)
    H = get_H(U, Uc)
    J = get_J(BHat, H)
    adjointU = get_adjointCand(J)
    if check_adjoint(L, U, adjointU, B)
        return adjointU
    else
        throw(error("Adjoint found not valid"))
    end
end

function get_adjointL(L::LinearDifferentialOperator)
    pFunctions, interval = L.pFunctions, L.interval
    D = Derivative()
    n = size(pFunctions)[2] - 1
    pFunctionsAdjoint = Array{Any}(undef, 1, n+1)
    for j=1:(n+1)
        a_j = 0
        for k=1:(n-j+2)
            if k == 1
                a_j += (-1)^(j-1) * binomial(k+j-2, k-1) * FunctionConjugate(pFunctions[n+2-(k+j-1)], interval)
            else
                a_j += (-1)^(k+j-2) * binomial(k+j-2, k-1) * (D^(k-1)*FunctionConjugate(pFunctions[n+2-(k+j-1)], interval))
            end
        end
        pFunctionsAdjoint[n+2-j] = a_j 
    end
    adjointL = LinearDifferentialOperator(pFunctionsAdjoint, interval)
    return adjointL
end

function check_selfAdjointness(L::LinearDifferentialOperator, U::VectorBoundaryForm)
    pFunctions = L.pFunctions
    
    adjointL = get_adjointL(L)
    pAdjointFunctions = adjointL.pFunctions
    
    if (all(i -> pFunctions[i] == pAdjointFunctions[i], 1:length(pFunctions))) == false
        return "The operator and its adjoint are not the same."
    else
        (a, b) = endpoints(L.interval)
        M, N = U.M, U.N
        Mstar, Nstar = M', N' 
        B = get_B(L)
        BEvalA = convert(Array{Complex}, evaluate.(B, a))
        BEvalB = convert(Array{Complex}, evaluate.(B, b))
        left = M * inv(BEvalA) * Mstar
        right = N * inv(BEvalB) * Nstar
        tol = set_tol(left, right)
        return all(i -> isapprox(left[i], right[i]; atol = tol), 1:length(left))
    end
end  


